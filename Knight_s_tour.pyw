import tkinter as tk

def encrypt_it():
    original_message = text_here.get("1.0", tk.END)
    text_here.delete('1.0', tk.END)
    original_message_separated = list(original_message)  # divide it into symbols
    original_message_length = len(original_message_separated)  # make it long enough
    while original_message_length % 64 != 0:
        original_message_separated.append(" ")
        original_message_length = original_message_length + 1

    while original_message_length != 0:
        # create a board
        #               0    1    2    3    4    5    6    7
        board_row_a = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_b = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_c = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_d = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_e = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_f = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_g = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_h = ["_", "_", "_", "_", "_", "_", "_", "_"]

        # There goes the Knight
        board_row_a[0] = original_message_separated[0]
        board_row_b[2] = original_message_separated[1]
        board_row_d[1] = original_message_separated[2]
        board_row_c[3] = original_message_separated[3]
        board_row_a[4] = original_message_separated[4]
        board_row_b[6] = original_message_separated[5]
        board_row_d[7] = original_message_separated[6]
        board_row_c[5] = original_message_separated[7]
        board_row_e[4] = original_message_separated[8]
        board_row_f[6] = original_message_separated[9]
        board_row_h[7] = original_message_separated[10]
        board_row_g[5] = original_message_separated[11]
        board_row_h[3] = original_message_separated[12]
        board_row_g[1] = original_message_separated[13]
        board_row_e[2] = original_message_separated[14]
        board_row_f[0] = original_message_separated[15]
        board_row_g[2] = original_message_separated[16]
        board_row_h[0] = original_message_separated[17]
        board_row_f[1] = original_message_separated[18]
        board_row_e[3] = original_message_separated[19]
        board_row_g[4] = original_message_separated[20]
        board_row_h[6] = original_message_separated[21]
        board_row_f[7] = original_message_separated[22]
        board_row_e[5] = original_message_separated[23]
        board_row_c[4] = original_message_separated[24]
        board_row_d[6] = original_message_separated[25]
        board_row_b[7] = original_message_separated[26]
        board_row_a[5] = original_message_separated[27]
        board_row_b[3] = original_message_separated[28]
        board_row_a[1] = original_message_separated[29]
        board_row_c[0] = original_message_separated[30]
        board_row_d[2] = original_message_separated[31]
        board_row_e[0] = original_message_separated[32]
        board_row_f[2] = original_message_separated[33]
        board_row_h[1] = original_message_separated[34]
        board_row_g[3] = original_message_separated[35]
        board_row_h[5] = original_message_separated[36]
        board_row_g[7] = original_message_separated[37]
        board_row_e[6] = original_message_separated[38]
        board_row_f[4] = original_message_separated[39]
        board_row_d[5] = original_message_separated[40]
        board_row_c[7] = original_message_separated[41]
        board_row_a[6] = original_message_separated[42]
        board_row_b[4] = original_message_separated[43]
        board_row_d[3] = original_message_separated[44]
        board_row_c[1] = original_message_separated[45]
        board_row_a[2] = original_message_separated[46]
        board_row_b[0] = original_message_separated[47]
        board_row_c[2] = original_message_separated[48]
        board_row_d[0] = original_message_separated[49]
        board_row_b[1] = original_message_separated[50]
        board_row_a[3] = original_message_separated[51]
        board_row_b[5] = original_message_separated[52]
        board_row_a[7] = original_message_separated[53]
        board_row_c[6] = original_message_separated[54]
        board_row_d[4] = original_message_separated[55]
        board_row_f[5] = original_message_separated[56]
        board_row_e[7] = original_message_separated[57]
        board_row_g[6] = original_message_separated[58]
        board_row_h[4] = original_message_separated[59]
        board_row_f[3] = original_message_separated[60]
        board_row_e[1] = original_message_separated[61]
        board_row_g[0] = original_message_separated[62]
        board_row_h[2] = original_message_separated[63]

        text_here.insert(tk.END, (' '.join(board_row_a)) + "\n" + \
                                 (' '.join(board_row_b)) + "\n" + \
                                 (' '.join(board_row_c)) + "\n" + \
                                 (' '.join(board_row_d)) + "\n" + \
                                 (' '.join(board_row_e)) + "\n" + \
                                 (' '.join(board_row_f)) + "\n" + \
                                 (' '.join(board_row_g)) + "\n" + \
                                 (' '.join(board_row_h)) + "\n")

        del original_message_separated[0:64]

def decrypt_it():
    unseal = text_here.get("1.0", tk.END)
    text_here.delete('1.0', tk.END)
    x = unseal.replace("\n", " ")
    encrypted_message_in_one_line = "".join(x)[::2]
    encrypted_message_separated = list(encrypted_message_in_one_line)
    encrypted_message_length = len(encrypted_message_separated)
    while encrypted_message_length != 0:
        # re-create a board
        #               0    1    2    3    4    5    6    7
        board_row_a = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_b = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_c = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_d = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_e = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_f = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_g = ["_", "_", "_", "_", "_", "_", "_", "_"]
        board_row_h = ["_", "_", "_", "_", "_", "_", "_", "_"]

        board_row_a[0] = encrypted_message_separated[0]
        board_row_a[1] = encrypted_message_separated[1]
        board_row_a[2] = encrypted_message_separated[2]
        board_row_a[3] = encrypted_message_separated[3]
        board_row_a[4] = encrypted_message_separated[4]
        board_row_a[5] = encrypted_message_separated[5]
        board_row_a[6] = encrypted_message_separated[6]
        board_row_a[7] = encrypted_message_separated[7]
        board_row_b[0] = encrypted_message_separated[8]
        board_row_b[1] = encrypted_message_separated[9]
        board_row_b[2] = encrypted_message_separated[10]
        board_row_b[3] = encrypted_message_separated[11]
        board_row_b[4] = encrypted_message_separated[12]
        board_row_b[5] = encrypted_message_separated[13]
        board_row_b[6] = encrypted_message_separated[14]
        board_row_b[7] = encrypted_message_separated[15]
        board_row_c[0] = encrypted_message_separated[16]
        board_row_c[1] = encrypted_message_separated[17]
        board_row_c[2] = encrypted_message_separated[18]
        board_row_c[3] = encrypted_message_separated[19]
        board_row_c[4] = encrypted_message_separated[20]
        board_row_c[5] = encrypted_message_separated[21]
        board_row_c[6] = encrypted_message_separated[22]
        board_row_c[7] = encrypted_message_separated[23]
        board_row_d[0] = encrypted_message_separated[24]
        board_row_d[1] = encrypted_message_separated[25]
        board_row_d[2] = encrypted_message_separated[26]
        board_row_d[3] = encrypted_message_separated[27]
        board_row_d[4] = encrypted_message_separated[28]
        board_row_d[5] = encrypted_message_separated[29]
        board_row_d[6] = encrypted_message_separated[30]
        board_row_d[7] = encrypted_message_separated[31]
        board_row_e[0] = encrypted_message_separated[32]
        board_row_e[1] = encrypted_message_separated[33]
        board_row_e[2] = encrypted_message_separated[34]
        board_row_e[3] = encrypted_message_separated[35]
        board_row_e[4] = encrypted_message_separated[36]
        board_row_e[5] = encrypted_message_separated[37]
        board_row_e[6] = encrypted_message_separated[38]
        board_row_e[7] = encrypted_message_separated[39]
        board_row_f[0] = encrypted_message_separated[40]
        board_row_f[1] = encrypted_message_separated[41]
        board_row_f[2] = encrypted_message_separated[42]
        board_row_f[3] = encrypted_message_separated[43]
        board_row_f[4] = encrypted_message_separated[44]
        board_row_f[5] = encrypted_message_separated[45]
        board_row_f[6] = encrypted_message_separated[46]
        board_row_f[7] = encrypted_message_separated[47]
        board_row_g[0] = encrypted_message_separated[48]
        board_row_g[1] = encrypted_message_separated[49]
        board_row_g[2] = encrypted_message_separated[50]
        board_row_g[3] = encrypted_message_separated[51]
        board_row_g[4] = encrypted_message_separated[52]
        board_row_g[5] = encrypted_message_separated[53]
        board_row_g[6] = encrypted_message_separated[54]
        board_row_g[7] = encrypted_message_separated[55]
        board_row_h[0] = encrypted_message_separated[56]
        board_row_h[1] = encrypted_message_separated[57]
        board_row_h[2] = encrypted_message_separated[58]
        board_row_h[3] = encrypted_message_separated[59]
        board_row_h[4] = encrypted_message_separated[60]
        board_row_h[5] = encrypted_message_separated[61]
        board_row_h[6] = encrypted_message_separated[62]
        board_row_h[7] = encrypted_message_separated[63]

        decrypted_message_separated = (
        board_row_a[0], board_row_b[2], board_row_d[1], board_row_c[3], board_row_a[4], board_row_b[6],
        board_row_d[7], board_row_c[5], board_row_e[4], board_row_f[6], board_row_h[7], board_row_g[5],
        board_row_h[3], board_row_g[1], board_row_e[2], board_row_f[0], board_row_g[2], board_row_h[0],
        board_row_f[1], board_row_e[3], board_row_g[4], board_row_h[6], board_row_f[7], board_row_e[5],
        board_row_c[4], board_row_d[6], board_row_b[7], board_row_a[5], board_row_b[3], board_row_a[1],
        board_row_c[0], board_row_d[2], board_row_e[0], board_row_f[2], board_row_h[1], board_row_g[3],
        board_row_h[5], board_row_g[7], board_row_e[6], board_row_f[4], board_row_d[5], board_row_c[7],
        board_row_a[6], board_row_b[4], board_row_d[3], board_row_c[1], board_row_a[2], board_row_b[0],
        board_row_c[2], board_row_d[0], board_row_b[1], board_row_a[3], board_row_b[5], board_row_a[7],
        board_row_c[6], board_row_d[4], board_row_f[5], board_row_e[7], board_row_g[6], board_row_h[4],
        board_row_f[3], board_row_e[1], board_row_g[0], board_row_h[2])

        text_here.insert(tk.END, (''.join(decrypted_message_separated)))

        del encrypted_message_separated[0:64]


# Interface
window = tk.Tk()
window.title("Knight's Tour")

text_here = tk.Text(window)
magic_buttons = tk.Frame(window)
btn_encrypt = tk.Button(magic_buttons, text="Encrypt", command = encrypt_it)
btn_decrypt = tk.Button(magic_buttons, text="Decrypt", command = decrypt_it)

btn_encrypt.grid(row=0, column=0, sticky="ew", padx=5)
btn_decrypt.grid(row=0, column=1, sticky="ew", padx=5)

magic_buttons.grid(row=1, column=0, sticky="ns")
text_here.grid(row=0, column=0, sticky="ns")

window.mainloop()
