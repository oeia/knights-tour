# Knights tour

A program to encrypt messages and decrypt the encrypted ones.

A user writes a text, presses a botton to encrypt it and then he/she can copy the encrypted message and send it to anyone else.

Or - a user recieves an encrypted message, puts it in the program, presses a botton to decrypt it and gets the original message.

## Author
Oleg Iansonas
https://gitlab.com/oeia
